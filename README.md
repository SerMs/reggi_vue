# 本项目已经全部优化完毕
本人已经部署到服务器了, 可以通过下面的链接访问
112.124.201.110

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Blog
## 个人博客 [SerMs](http://www.serms.top)

湘约楼项目转Vue脚手架
> **5/18---5/28**
> 思路分析及基本框架搭建
>  1. 配置饿了么组件库
>     1.1 设置全局组件库
>  2. 封装ajax请求
>     1. 封装`request`方法
>     2. 封装`respons`方法
>     3. 封装响应数据处理
>     4. 封装接口地址
>  3. 配置路由
>    1. 基本路由 home/about/blog/contact/404
>    3. 嵌套路由 home下的所有子组件
>    4. 全局路由 home
>  4. 配置Vuex store-->vuex
>  5. 配置代理跨域
>  6. 配置全局过滤器
>  7. .....
> **5/29---6/4**
> 
> 完善员工页面和分类页面
> **6/4---6/6**
> 完善套餐和菜品



未解决的细节问题
1. 数据刷新页面未联动  (已解决)
2. 非管理员登录下,员工编辑功能不完善   (已解决)
3. 子路由不能在父路由中上下滚动(已解决)
   1. 解决方法: 在子路由中添加    height: 100%;  overflow-y: auto !important;
4. 页面切换标题未改变(已解决)
5. 