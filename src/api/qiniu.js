/* 七牛上传接口 */
/* ******************************************************* */
import request from "./request";

//文件上传
export const qiniuUpload = (params) => request({ url: "/qiniukodo/upload", method: 'post', data: params })