/* 分类管理接口 */
/* ******************************************************* */
import request from "./request";

// 查询列表接口
export const getDishPage = (params) => request({ url: '/dish/page', method: 'get', params });

// 删除接口
export const deleteDish = (ids) => request({ url: '/dish', method: 'delete', params: { ids } })

// 修改接口
export const editDish = (params) => request({ url: '/dish', method: 'put', data: { ...params } });

// 新增接口
export const addDish = (params) => { return request.post("/dish", params); };

// 查询详情
export const queryDishById = (id) => { return request.get(`/dish/${id}`); };

// 获取菜品分类列表
export const getCategoryList = (params) => request({ url: '/category/list', method: 'get', params });

// 查菜品列表的接口
export const queryDishList = (params) => request({ url: '/dish/list', method: 'get', params });

// 查询所有分类名称
export const categoiryList = () => { return request.get("/dish/categoiryList"); }

// 文件down预览
export const commonDownload = (params) => { return request.get("/common/download", params).headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }); };

// 起售停售---批量起售停售接口
export const dishStatusByStatus = (params) => request({ url: `/dish/status/`, method: 'post', params: { status: params.status, ids: params.id } })

//文件上传 废弃!!!! 改用qiniuUpload
export const dishUpload = (params) => { return request.post("/common/upload", params) };

//图片回显
export const dishDownload = (params) => { return request.get(`/common/download?name=${params}`, { responseType: 'arraybuffer' }) } 