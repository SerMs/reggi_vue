//登录接口
import request from "./request";

//测试接口
// export const reqCategoryList = () => request.get("/product/getBaseCategoryList");

//退出登录
export const logoutApi = () => request.post("/employee/logout");

//登录
export const loginApi = (data) => request.post("/employee/login", data);