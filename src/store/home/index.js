//store模块仓库
//引入路由模块
import router from '@/router'

const state = {
    menuActived: '2',
    headTitle: '员工管理',
    goBackFlag: false,
    loading: false,     //到时候要改成true
    timer: null,
}
const mutations = {
    setHeadTitle(state, headTitle) {
        state.headTitle = headTitle;
    },
    CLOSE_LOADING(state) {
        state.timer = null;
        state.timer = setTimeout(() => {
            state.loading = false
        }, 1000)
    },
    MENU_HANDLE(state, data) {
        state.loading = true
        state.menuActived = data.id
        router.push('/' + data.url)
        state.headTitle = data.name
        console.log(state.headTitle);
        state.goBackFlag = data.goBackFlag
    },
    TIME_CLEAR_TIMEOUT(state) {
        state.timer = null;
        clearTimeout(state.timer)
    }

}
const actions = {
    setHeadTitle({ commit }, headTitle) {
        commit('setHeadTitle', headTitle)

    },
    menuHandle({ commit }, data) {
        commit('MENU_HANDLE', data)
        commit('CLOSE_LOADING')
    },
    closeLoading({ commit }) {
        commit('CLOSE_LOADING')
    },
    timerClearTimeout({ commit }) {
        commit('TIME_CLEAR_TIMEOUT')
    }
}
const getters = {}
export default {
    state,
    mutations,
    actions,
    getters
}