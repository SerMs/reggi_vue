import {
    Button, Message, Row,
    scrollbar,
    menu

} from 'element-ui'
export default {
    install(Vue) {
        Vue.use(Button)
        // Vue.use(Message)
        Vue.use(Row)

        //可使用this.$message
        // Vue.prototype.$message = Message
    }
}