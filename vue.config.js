const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  devServer: {
    port: 8080,
    host: 'localhost',
    //代理跨域
    proxy: {
      '/api': {
        target: 'http://localhost:8888',
        // target: 'http://192.168.174.100:8888',
        // target: 'http://47.99.194.162:8888',
        changeOrigin: true, // 支持跨域
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  publicPath: "./",// 基本路径

})
